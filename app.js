/*
  Negin Mashreghi
  April 2019
  negin_mashreghi@yahoo.com
*/
const express = require('express');
const path = require('path');

const app = express();

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname+'/index.html'));
});

const port = 3000;
app.listen(port, () => console.log(`Server started on port ${port}`)); 

module.exports = app;